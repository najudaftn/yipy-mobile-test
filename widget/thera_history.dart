import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:resident/src/assets/icons_path.dart';
import 'package:resident/src/modules/thera/thera_detail.dart';
import 'package:resident/src/modules/thera/thera_model.dart';
import 'package:resident/src/utils/navigation_utils.dart';
import 'package:resident/src/utils/string_util.dart';
import 'package:resident/src/widgets2/divider.dart';

class TheraHistory extends StatelessWidget {
  final List<TransactionHistory> histories;
  const TheraHistory({super.key, required this.histories});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: EdgeInsets.only(left: 16),
            child: Text("Transaction History", style: TextStyle(fontWeight: FontWeight.bold)),
          ),
          SizedBox(height: 10),
          CustomDivider(horizontal: 16),
          Column(
            children: List.generate(histories.length, (i) {
              return ListTile(
                leading: CircleAvatar(backgroundImage: AssetImage(IconAsset.token_listrik_yellow)),
                title: Text("Nominal Rp ${moneyFormatter(histories[i].amount).nonSymbol}"),
                subtitle: Text("${DateFormat("d MMM y, HH:mm").format(DateTime.parse(histories[i].paymentDate!))} WIB"),
                trailing: Text(
                  "Rp ${moneyFormatter(histories[i].amount! + histories[i].paymentFee!).nonSymbol}",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                onTap: () {
                  Navigate.to(context, TheraDetailPage(trxCode: histories[i].trxCode!));
                },
              );
            }),
          )
        ],
      ),
    );
  }
}
