import 'package:resident/src/modules/store/payment/method/payment_method_page.dart';
import 'package:resident/src/modules/thera/thera_vm.dart';
import 'package:resident/src/modules/thera/widget/thera_history.dart';
import 'package:resident/src/modules/thera/widget/thera_info.dart';
import 'package:resident/src/utils/default_widget.dart';
import 'package:resident/src/utils/navigation_utils.dart';
import 'package:resident/src/utils/string_util.dart';
import 'package:resident/src/widgets2/divider.dart';
import 'package:resident/src/widgets2/keyval.dart';

class TheraPage extends StatefulWidget {
  const TheraPage({super.key});

  @override
  State<TheraPage> createState() => _TheraPageState();
}

class _TheraPageState extends State<TheraPage> {
  @override
  void initState() {
    context.read<TheraVm>().getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(elevation: 0, title: Text('Token Listrik')),
      body: Consumer<TheraVm>(
        builder: (context, vm, _) {
          if (vm.isLoading) {
            return Center(child: CircularProgressIndicator());
          }
          if (vm.thera == null) {
            return Center(child: Text("something went wrong"));
          }
          return ListView(
            children: [
              Container(
                padding: EdgeInsets.all(16),
                child: Column(
                  children: [
                    keyValue("No. Meter", vm.thera!.meterNumber.toString()),
                    SizedBox(height: 4),
                    keyValue("Unit", vm.thera!.unitNumber.toString()),
                  ],
                ),
              ),
              CustomDivider(horizontal: 16),
              Container(
                padding: EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text('Pilih Nominal Token', style: TextStyle()),
                    SizedBox(height: 15),
                    GridView.builder(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 10,
                        childAspectRatio: 2.1,
                      ),
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: vm.thera!.priceList!.length,
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () => vm.setselectedNominal(index, vm.thera!.priceList![index].amount!),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: vm.selected.index == index ? Colors.red.withOpacity(0.1) : null,
                              border: Border.all(color: vm.selected.index == index ? Colors.red : Colors.grey),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "Rp ${moneyFormatter(vm.thera!.priceList![index].amount).nonSymbol}",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                SizedBox(height: 3),
                                Text(
                                  "${vm.thera!.priceList![index].kwh} kWh",
                                  style: TextStyle(fontSize: 13),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                    TheraInfo(),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(elevation: 0),
                      onPressed: vm.isPaymentProcessing ? null : () async => await vm.payment(context),
                      child: Text(vm.isPaymentProcessing ? "Proses ... " : "Pilih Pembayaran"),
                    )
                  ],
                ),
              ),
              SizedBox(height: 10),
              TheraHistory(histories: vm.thera!.transactionHistory!),
            ],
          );
        },
      ),
    );
  }
}
