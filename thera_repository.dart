import 'dart:convert';
import 'package:resident/src/api/constant/module.dart';
import 'package:resident/src/api/engine/api.dart';
import 'package:resident/src/api/model/api_error.dart';
import 'package:resident/src/modules/thera/thera_model.dart';

class TheraRepository with RemoteRepository {
  Future<dynamic> getData({required int unitId}) async {
    Uri request = Uri.https(BASE_URL, "$TRANSACTION_MODULE/resident/thera", {"unit_id": unitId.toString()});
    final response = await get(request);
    return response is ApiError ? response : Thera.fromJson(response['data']);
  }

  Future<dynamic> getDetail({required String trx_code}) async {
    Uri request = Uri.https(BASE_URL, "$TRANSACTION_MODULE/resident/thera/detail", {"trx_code": trx_code});
    final response = await get(request);
    return response is ApiError ? response : TheraDetail.fromJson(response['data']);
  }

  Future<dynamic> createTrx({required String meterNumber, required int unitId, required int amount}) async {
    final Uri request = Uri.https(BASE_URL, "$TRANSACTION_MODULE/resident/thera");
    final body = json.encode({"meter_number": meterNumber, "unit_id": unitId, "amount": amount});
    final response = await post(request, body: body);
    return response is ApiError ? response : response['trx_code'];
  }

  Future<dynamic> checkOutstanding() async {
    final Uri request = Uri.https(BASE_URL, "$BILLING_MODULE/resident/check-outstanding");
    final response = await get(request);
    return response is ApiError ? response : response['data'];
  }
}
