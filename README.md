# Yipy Test Mobile
### Harap repository ini di fork

Kode ini adalah sebuah module untuk pembelian token listrik, dimana terdapat:
- pilihan harga 
- history pembelian
- button untuk melakukan pembelian

<img src="https://test-yipy.s3.ap-southeast-1.amazonaws.com/token+listrik.jpeg" alt="Token Listrik" width="250"/>

## Test Case

Silakan gunakan kode ini sebagai referensi untuk membuat project mobile flutter

harap buat kode dan lampirkan screenshoot untuk membuat module token listrik versi anda.