import 'package:resident/src/flavor/flavors.dart';
import 'package:resident/src/modules/home/home_notifier.dart';

bool showThera(HomeNotifier? notifier) {
  if (notifier == null || notifier.selectedUnit == null) {
    return false;
  }
  if (F.appFlavor == Flavor.development && notifier.selectedUnit!.buildingId == 7) {
    return true;
  }

  if (F.appFlavor == Flavor.production && notifier.selectedUnit!.buildingId == 36) {
    return true;
  }

  return false;
}
