// ignore_for_file: use_build_context_synchronously

import 'package:resident/src/assets/images_path.dart';
import 'package:resident/src/modules/billing/billing_page.dart';
import 'package:resident/src/modules/main/main_page.dart';
import 'package:resident/src/modules/store/payment/method/payment_method_page.dart';
import 'package:resident/src/modules/thera/thera_model.dart';
import 'package:resident/src/modules/thera/thera_repository.dart';
import 'package:resident/src/preference/preference_helper.dart';
import 'package:resident/src/utils/default_widget.dart';
import 'package:resident/src/utils/navigation_utils.dart';
import 'package:resident/src/widgets/bottom_dialog_message.dart';
import 'package:resident/src/widgets/primary_button.dart';

import '../../libraries/func/space_string.dart';

class TheraVm extends ChangeNotifier {
  Thera? thera;
  TheraDetail? theraDetail;
  bool usePeymentForThera = false;
  bool isLoading = false;
  bool isDetailLoading = false;
  bool isPaymentProcessing = false;

  late SelectedPriceList selected;

  String get tokenCode => theraDetail!.tokenCode!.isEmpty ? "" : theraTokenFormatter(theraDetail!.tokenCode!);

  void setselectedNominal(int index, int amount) {
    selected.index = index;
    selected.amount = amount;
    notifyListeners();
  }

  Future<void> getData() async {
    isLoading = true;
    thera = null;
    final unit = await PreferenceHelper.getUnit();
    final response = await TheraRepository().getData(unitId: unit!.unitId!);
    if (response is Thera) {
      thera = response;

      selected = SelectedPriceList(
        index: 0,
        meterNumber: thera!.meterNumber,
        unitId: unit.unitId,
        amount: thera!.priceList!.first.amount,
      );
      notifyListeners();
    }
    isLoading = false;
    notifyListeners();
  }

  Future<void> payment(BuildContext context) async {
    // Get Trx Code;
    isPaymentProcessing = true;
    notifyListeners();

    final isOutstanding = await TheraRepository().checkOutstanding();

    if (isOutstanding) {
      showModalBottomSheet(
        context: context,
        builder: (_) => Container(
          height: 270,
          padding: EdgeInsets.symmetric(
            vertical: spacing_large,
            horizontal: spacing_medium,
          ),
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.vertical(top: Radius.circular(spacing_large)),
            color: Colors.white,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 40, child: Image.asset(ImageAssets.logo_full_color)),
              SizedBox(height: 20),
              Text(
                "Kamu Punya Tagihan Yang Belum Dibayar",
                style: TextStyle(fontWeight: FontWeight.w500),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 5),
              Text(
                "Yuk bayar dahulu tagihan kamu untuk dapat mengakses fitur ini.",
                style: TextStyle(color: Colors.grey),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 30),
              PrimaryButton(
                text: 'Bayar Sekarang',
                callback: () {
                  Navigate.to(context, BillingPage());
                },
              )
            ],
          ),
        ),
      );

      isPaymentProcessing = false;
      notifyListeners();
      return;
    }

    final trxCode = await TheraRepository().createTrx(meterNumber: selected.meterNumber!, unitId: selected.unitId!, amount: selected.amount!);

    isPaymentProcessing = false;
    notifyListeners();

    if (trxCode is String) {
      usePeymentForThera = true;
      Navigate.to(context, PaymentMethodPage(trxCode: trxCode));
    }
  }

  Future<void> getDetail(String trxCode) async {
    isDetailLoading = true;
    theraDetail = null;
    final response = await TheraRepository().getDetail(trx_code: trxCode);

    if (response is TheraDetail) {
      theraDetail = response;
    }
    isDetailLoading = false;
    notifyListeners();
  }
}
