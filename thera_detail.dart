import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:resident/src/modules/thera/thera_vm.dart';
import 'package:resident/src/modules/thera/widget/thera_404.dart';
import 'package:resident/src/utils/default_widget.dart';
import 'package:resident/src/utils/string_util.dart';
import 'package:resident/src/utils/toast.dart';
import 'package:resident/src/widgets2/divider.dart';
import 'package:resident/src/widgets2/keyval.dart';

class TheraDetailPage extends StatefulWidget {
  final String trxCode;
  const TheraDetailPage({super.key, required this.trxCode});

  @override
  State<TheraDetailPage> createState() => _TheraDetailPageState();
}

class _TheraDetailPageState extends State<TheraDetailPage> {
  @override
  void initState() {
    context.read<TheraVm>().getDetail(widget.trxCode);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefaultAppBar(elevation: 0, title: Text('Transaction Detail')),
      body: Consumer<TheraVm>(
        builder: (context, vm, _) {
          if (vm.isDetailLoading) {
            return Center(child: CircularProgressIndicator());
          }
          if (vm.theraDetail == null) {
            return TheraNotFound();
          }
          return ListView(
            children: [
              Card(
                elevation: 0,
                margin: EdgeInsets.zero,
                child: Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Container(margin: EdgeInsets.only(bottom: 5), child: Text("Rp")),
                              SizedBox(width: 2),
                              Text(
                                moneyFormatter(vm.theraDetail!.totalAmount).nonSymbol,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 28,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(height: 20),
                      CustomDivider(),
                      keyValue("No. Meter", vm.theraDetail!.meterNumber.toString()),
                      SizedBox(height: 10),
                      keyValue("Unit", vm.theraDetail!.unitNumber.toString()),
                      SizedBox(height: 5),
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(25),
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(color: Colors.grey[200], borderRadius: BorderRadius.circular(10)),
                child: vm.theraDetail!.tokenCode!.isEmpty
                    ? Text(
                        "Token Sedang Diproses. Silahkan cek riwayat transaksi untuk melihat nomor token.",
                        textAlign: TextAlign.center,
                      )
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text("Token :", style: TextStyle(color: Colors.grey)),
                          SizedBox(height: 5),
                          Text(
                            vm.tokenCode,
                            style: TextStyle(color: Colors.red, fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(height: 20),
                          SizedBox(
                            width: 140,
                            height: 30,
                            child: OutlinedButton(
                                style: OutlinedButton.styleFrom(side: BorderSide(color: color_primary)),
                                onPressed: () async {
                                  await Clipboard.setData(new ClipboardData(text: vm.tokenCode));
                                  showToast('Copied to Clipboard');
                                },
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text("Salin Token", style: TextStyle(fontSize: 13)),
                                    SizedBox(width: 5),
                                    Icon(Icons.copy, size: 16),
                                  ],
                                )),
                          ),
                          SizedBox(height: 10),
                        ],
                      ),
              ),
              Card(
                elevation: 0,
                margin: EdgeInsets.zero,
                child: Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      keyValue("No. Transaksi", vm.theraDetail!.tokenCode!),
                      CustomDivider(vertical: 15),
                      keyValue("Waktu", "${DateFormat("d MMM y, HH:mm").format(DateTime.parse(vm.theraDetail!.paymentDate!))}"),
                      CustomDivider(vertical: 15),
                      keyValue("Jumlah Tagihan", "Rp ${moneyFormatter(vm.theraDetail!.amount).nonSymbol}"),
                      CustomDivider(vertical: 15),
                      keyValue("Convenience fee", "Rp ${moneyFormatter(vm.theraDetail!.paymentFee).nonSymbol}"),
                      CustomDivider(vertical: 15),
                      keyValue("Payment Method", vm.theraDetail!.paymentBank!.toUpperCase()),
                      CustomDivider(),
                    ],
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
